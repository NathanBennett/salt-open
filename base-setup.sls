##Base installation for all servers.

monitoring:
  pkg.installed:
    - name: htop

compression:
  pkg.installed:
    - pkgs:
      - zip
      - p7zip