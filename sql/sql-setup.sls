mydatabases:
  pkg.installed:
    - name: mysql-server

SQL-cmd-Setup:
  cmd.script:
    - source: salt://sql/sql-setup.sh
    - cwd: /home
    - user: salt