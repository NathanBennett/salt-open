## Using the naming convention to setup these servers, You could use nodegroups in /etc/salt/master
## or Grains if you want to set this up. This is just the easiest to setup.

base:
  '*':
    - base-setup
  '*web*':
    - web.webserver
  '*sql*':
    - sql.sql-setup