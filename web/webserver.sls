web-packages:
  pkg.installed:
    - pkgs:
      - apache2
      - php
      - php-mysql
      - libapache2-mod-php
      - mysql-client

web-setup:
  cmd.script:
    - source: salt://web/web-setup.sh
    - cwd: /home
    - user: salt